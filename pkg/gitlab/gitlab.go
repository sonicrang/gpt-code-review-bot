package gitlab

import (
	"fmt"
	"github.com/xanzy/go-gitlab"
)

// Config GitLab config
type Config struct {
	Host   string
	Token  string
	Client *gitlab.Client // 存储GitLab客户端实例
}

// NewGitLabClient Create a new GitLab client
func NewGitLabClient(token, host string) (*Config, error) {
	urlStr := fmt.Sprintf("%s/api/v4", host)
	client, err := gitlab.NewClient(token, gitlab.WithBaseURL(urlStr))
	if err != nil {
		return nil, err
	}
	return &Config{
		Host:   host,
		Token:  token,
		Client: client,
	}, nil
}

// GitLabCreateMergeRequestDiscussion Create a new discussion to a merge request
func (g *Config) GitLabCreateMergeRequestDiscussion(projectId, mergeRequestId int, discussion string) error {
	opt := &gitlab.CreateMergeRequestDiscussionOptions{
		Body: &discussion,
		//Position: &position,
	}
	_, _, err := g.Client.Discussions.CreateMergeRequestDiscussion(projectId, mergeRequestId, opt)
	if err == nil {
		return nil
	}
	return err
}

// GitLabAddMergeRequestDiscussionNote Add a new note to an existing discussion
func (g *Config) GitLabAddMergeRequestDiscussionNote(projectId, mergeRequestId int, discussionId, discussion string) error {
	opt := &gitlab.AddMergeRequestDiscussionNoteOptions{
		Body: &discussion,
	}
	_, _, err := g.Client.Discussions.AddMergeRequestDiscussionNote(projectId, mergeRequestId, discussionId, opt)
	if err == nil {
		return nil
	}
	return err
}

// GitLabUpdateMergeRequestDiscussionNote Update an existing discussion note
func (g *Config) GitLabUpdateMergeRequestDiscussionNote(projectId, mergeRequestId, noteId int, discussionId, discussion string) error {
	opt := &gitlab.UpdateMergeRequestDiscussionNoteOptions{
		Body: &discussion,
	}
	_, _, err := g.Client.Discussions.UpdateMergeRequestDiscussionNote(projectId, mergeRequestId, discussionId, noteId, opt)
	if err == nil {
		return nil
	}
	return err
}

// GitLabGetMergeRequestChanges Get single merge request changes
func (g *Config) GitLabGetMergeRequestChanges(projectId, mergeRequestId int) (*gitlab.MergeRequest, error) {
	opt := &gitlab.GetMergeRequestChangesOptions{}
	changes, _, err := g.Client.MergeRequests.GetMergeRequestChanges(projectId, mergeRequestId, opt)
	if err != nil {
		return nil, err
	}
	return changes, nil
}

// GitLabGetMergeRequestVersion Get the latest merge request version
func (g *Config) GitLabGetMergeRequestVersion(projectId, mergeRequestId int) ([]*gitlab.MergeRequestDiffVersion, error) {
	opt := &gitlab.GetMergeRequestDiffVersionsOptions{}
	diffVersions, _, err := g.Client.MergeRequests.GetMergeRequestDiffVersions(projectId, mergeRequestId, opt)
	if err != nil {
		return nil, err
	}
	return diffVersions, nil
}

// GetLatestMergeRequestVersion Get the latest merge request version
func (g *Config) GetLatestMergeRequestVersion(projectId, mergeRequestId int) (*gitlab.MergeRequestDiffVersion, error) {
	diffVersions, err := g.GitLabGetMergeRequestVersion(projectId, mergeRequestId)
	if err != nil {
		return nil, err
	}
	if len(diffVersions) == 0 {
		return nil, fmt.Errorf("no diff version found")
	}
	version, _, err := g.Client.MergeRequests.GetSingleMergeRequestDiffVersion(projectId, mergeRequestId, diffVersions[0].ID)
	if err != nil {
		return nil, err
	}
	return version, nil
}
