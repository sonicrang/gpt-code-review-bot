package azure

import (
	"context"
	"fmt"
	"strings"
	"time"

	openai "github.com/sashabaranov/go-openai"
)

type CompletionConfig struct {
	MaxTokens        int            `json:"max_tokens,omitempty"`
	Temperature      float32        `json:"temperature,omitempty"`
	TopP             float32        `json:"top_p,omitempty"`
	N                int            `json:"n,omitempty"`
	Stream           bool           `json:"stream,omitempty"`
	LogProbs         int            `json:"logprobs,omitempty"`
	Echo             bool           `json:"echo,omitempty"`
	Stop             []string       `json:"stop,omitempty"`
	PresencePenalty  float32        `json:"presence_penalty,omitempty"`
	FrequencyPenalty float32        `json:"frequency_penalty,omitempty"`
	BestOf           int            `json:"best_of,omitempty"`
	LogitBias        map[string]int `json:"logit_bias,omitempty"`
}

// Client is a client for Azure OpenAI.
type Chat struct {
	Client           *openai.Client
	CompletionConfig *CompletionConfig
}

func NewChat(apiKey, resourceName, modelType, deploymentName, apiVersion string, completionConfig *CompletionConfig) *Chat {
	fmt.Println("[access to azure openai]")
	baseUrl := "https://" + resourceName + ".openai.azure.com/"
	config := openai.DefaultAzureConfig(apiKey, baseUrl)

	if deploymentName != "" {
		defaultModelType := "gpt-3.5-turbo"
		if modelType != "" {
			defaultModelType = modelType
		}
		config.AzureModelMapperFunc = func(model string) string {
			azureModelMapping := map[string]string{
				defaultModelType: deploymentName,
			}
			return azureModelMapping[model]
		}
	}

	if apiVersion != "" {
		config.APIVersion = apiVersion
	}

	client := openai.NewClientWithConfig(config)
	return &Chat{
		Client:           client,
		CompletionConfig: completionConfig,
	}
}

func (c *Chat) SendMessage(prompt string) (string, error) {
	fmt.Println("[request to azure openai]")
	resp, err := c.Client.CreateChatCompletion(
		context.Background(),
		openai.ChatCompletionRequest{
			Model: openai.GPT3Dot5Turbo,
			Messages: []openai.ChatCompletionMessage{
				{
					Role:    openai.ChatMessageRoleUser,
					Content: prompt,
				},
			},
			MaxTokens:        c.CompletionConfig.MaxTokens,
			Temperature:      c.CompletionConfig.Temperature,
			TopP:             c.CompletionConfig.TopP,
			N:                c.CompletionConfig.N,
			Stream:           c.CompletionConfig.Stream,
			Stop:             c.CompletionConfig.Stop,
			PresencePenalty:  c.CompletionConfig.PresencePenalty,
			FrequencyPenalty: c.CompletionConfig.FrequencyPenalty,
			LogitBias:        c.CompletionConfig.LogitBias,
		},
	)
	if err != nil {
		return "", err
	}

	return resp.Choices[0].Message.Content, nil
}

func (c *Chat) CodeReview(prompt string) (string, error) {

	if prompt == "" {
		return ":warning: diff code is empty", nil
	}
	start := time.Now()
	tokenNum := len(strings.ReplaceAll(prompt, " ", ""))
	fmt.Println("token: ", tokenNum)

	res, err := c.SendMessage(prompt)
	if err != nil {
		return "", err
	}
	elapsed := time.Since(start)
	fmt.Printf("code-review cost: %v\n", elapsed)
	return res, nil
}
