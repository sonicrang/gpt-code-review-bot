/*
Copyright © 2023 guoxudong

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

	http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cmd

import (
	"crypto/sha1"
	"encoding/hex"
	"encoding/json"
	"fmt"
	"log"
	"os"

	"gpt-code-review-bot/pkg/gitlab"

	chat "gpt-code-review-bot/pkg/openai"

	azure "gpt-code-review-bot/pkg/azure"

	"github.com/spf13/cobra"
	"github.com/spf13/viper"
)

var (
	Defaultlanguage = "English"
	DefaultPrompt   = "Review the below-mentioned code and provide some suggestions in %s language with regards to better readability, " +
		"performance, and security, or any other recommends. If there are any recommendation, kindly provide the sample code. " +
		"For the code part, please use two ``` wrappers. In the following, + represents a new line of code and - a reduced line of code.\n "
	DiffPrompt = "```diff\n%s\n```"
)

type CodeReviewParams struct {
	ProjectID        int
	MergeRequestID   int
	DiscussionID     string
	NoteId           int
	NewPath          string
	URL              string
	Language         string
	Prompt           string
	Auth             *Auth
	CompletionConfig *chat.CompletionConfig
}

type Auth struct {
	Type           string
	ApiKey         string
	ResourceName   string
	ModelType      string
	DeploymentName string
	ApiVersion     string
}

// rootCmd represents the base command when called without any subcommands
var rootCmd = &cobra.Command{
	Use: "gpt-code-review-bot",
	RunE: func(cmd *cobra.Command, args []string) error {

		path := viper.GetString("TRIGGER_PAYLOAD")

		fmt.Println("[receive note webhook]")

		c, err := readWebhookFile(path)
		if err != nil {
			return err
		}

		fmt.Println("[unmarshal note webhook]")

		// Filter quick action
		if c.ObjectAttributes.NoteableType != "MergeRequest" {
			fmt.Println("[skip without mergerequest]")
			return nil
		}

		fmt.Println("[get gitlab variables]")

		config, err := gitlab.NewGitLabClient(
			viper.GetString("GITLAB_TOKEN"),
			viper.GetString("GITLAB_HOST"))
		if err != nil {
			return err
		}

		fmt.Println("[get openai variables]")
		auth := Auth{}
		switch viper.GetString("API_TYPE") {
		case "openai":
			auth.Type = "openai"
			auth.ApiKey = viper.GetString("OPENAI_API_KEY")
		case "azure":
			auth.Type = "azure"
			auth.ApiKey = viper.GetString("AZURE_API_KEY")
			auth.ResourceName = viper.GetString("AZURE_RESOURCE_NAME")
			auth.ModelType = viper.GetString("AZURE_MODEL_TYPE")
			auth.DeploymentName = viper.GetString("AZURE_DEPLOYMENT_NAME")
			auth.ApiVersion = viper.GetString("AZURE_API_VERSION")

		default:
			auth.Type = "openai"
			auth.ApiKey = viper.GetString("OPENAI_API_KEY")
		}

		fmt.Println("[get completion variables]")
		completionConfig, err := readCompletionConfig()
		if err != nil {
			return err
		}

		codeReviewParams := CodeReviewParams{
			Auth:             &auth,
			Language:         viper.GetString("LANGUAGE"),
			Prompt:           viper.GetString("PROMPT"),
			ProjectID:        c.ProjectId,
			MergeRequestID:   c.MergeRequest.Iid,
			DiscussionID:     c.ObjectAttributes.DiscussionId,
			NoteId:           c.ObjectAttributes.Id,
			NewPath:          c.ObjectAttributes.OriginalPosition.NewPath,
			URL:              c.MergeRequest.Url,
			CompletionConfig: completionConfig,
		}

		fmt.Println("[choose action]")

		switch c.ObjectAttributes.Note {
		case "/review-all":
			return codeReviewParams.CodeReviewAll(config)
		case "/review":
			if c.ObjectAttributes.Type != "DiffNote" {
				break
			}
			return codeReviewParams.CodeReviewSingle(config)
		case "/review-r":
			if c.ObjectAttributes.Type != "DiffNote" {
				break
			}
			return codeReviewParams.CodeReviewSingleReplace(config)
		default:
			fmt.Println("skip")
		}

		fmt.Println("[action complete]")
		return nil
	},
}

// Execute adds all child commands to the root command and sets flags appropriately.
// This is called by main.main(). It only needs to happen once to the rootCmd.
func Execute() {
	err := rootCmd.Execute()
	if err != nil {
		os.Exit(1)
	}
}

func init() {
	cobra.OnInitialize(initConfig)
}

// initConfig reads in config file and ENV variables if set.
func initConfig() {
	viper.AutomaticEnv() // read in environment variables that match
}

func (c *CodeReviewParams) CodeReviewAll(g *gitlab.Config) error {
	version, err := g.GetLatestMergeRequestVersion(c.ProjectID, c.MergeRequestID)
	if err != nil {
		return err
	}
	fmt.Println("review file num: ", len(version.Diffs))
	for _, d := range version.Diffs {
		if d.DeletedFile {
			continue
		}
		c.NewPath = d.NewPath
		review, err := c.getDiffReview(d.Diff)
		if err != nil {
			return err
		}
		err = g.GitLabCreateMergeRequestDiscussion(c.ProjectID, c.MergeRequestID, review)
		if err != nil {
			return err
		}
	}
	return err
}

func (c *CodeReviewParams) CodeReviewSingle(g *gitlab.Config) error {
	version, err := g.GetLatestMergeRequestVersion(c.ProjectID, c.MergeRequestID)
	if err != nil {
		return err
	}
	for _, d := range version.Diffs {
		if d.NewPath == c.NewPath {
			review, err := c.getDiffReview(d.Diff)
			if err != nil {
				return err
			}
			return g.GitLabAddMergeRequestDiscussionNote(c.ProjectID, c.MergeRequestID, c.DiscussionID, review)
		}
	}
	return nil
}

func (c *CodeReviewParams) CodeReviewSingleReplace(g *gitlab.Config) error {
	version, err := g.GetLatestMergeRequestVersion(c.ProjectID, c.MergeRequestID)
	if err != nil {
		return err
	}
	for _, d := range version.Diffs {
		if d.NewPath == c.NewPath {
			review, err := c.getDiffReview(d.Diff)
			if err != nil {
				return err
			}
			return g.GitLabUpdateMergeRequestDiscussionNote(c.ProjectID, c.MergeRequestID, c.NoteId, c.DiscussionID, review)
		}
	}
	return nil
}

func (c *CodeReviewParams) getDiffReview(diff string) (string, error) {
	prompt := c.generatePrompt(diff)

	review, err := c.sendToAi(prompt)
	if err != nil || review == "" {
		return "", err
	}
	diffLink := fmt.Sprintf("%s/diffs#diff-content-%s", c.URL, getSHA1(c.NewPath))
	comment := fmt.Sprintf("## [%s](%s)\n> Review by %s   \n\n%s", c.NewPath, diffLink, c.Auth.Type, review)
	return comment, nil
}

func getSHA1(name string) string {
	sha1Hash := sha1.Sum([]byte(name))
	return hex.EncodeToString(sha1Hash[:])
}

func readWebhookFile(name string) (*gitlab.NoteHook, error) {

	data, err := os.ReadFile(name)
	if err != nil {
		return nil, err
	}
	// Unmarshal the data into a Person object.
	var comment *gitlab.NoteHook
	if err := json.Unmarshal(data, &comment); err != nil {
		log.Fatal(err)
	}

	return comment, nil
}

func (c *CodeReviewParams) generatePrompt(patch string) string {
	if c.Prompt == "" {
		c.Prompt = DefaultPrompt
	}
	if c.Language == "" {
		c.Language = Defaultlanguage
	}
	return fmt.Sprintf(c.Prompt, c.Language) + fmt.Sprintf(DiffPrompt, patch)
}

func (c *CodeReviewParams) sendToAi(prompt string) (string, error) {
	var review string
	var err error
	switch c.Auth.Type {
	case "openai":
		review, err = chat.NewChat(c.Auth.ApiKey, c.CompletionConfig).CodeReview(prompt)
	case "azure":
		review, err = azure.NewChat(c.Auth.ApiKey, c.Auth.ResourceName, c.Auth.ModelType, c.Auth.DeploymentName, c.Auth.ApiVersion, (*azure.CompletionConfig)(c.CompletionConfig)).CodeReview(prompt)
	default:
		return "", fmt.Errorf("invalid api type")
	}
	if err != nil || review == "" {
		return "", err
	}
	return review, nil
}

func readCompletionConfig() (*chat.CompletionConfig, error) {
	completionConfig := chat.CompletionConfig{}
	if viper.GetString("COMPLETION_CONFIG") != "" {
		config, err := os.ReadFile(viper.GetString("COMPLETION_CONFIG"))
		if err != nil {
			return nil, err
		}
		err = json.Unmarshal(config, &completionConfig)
		if err != nil {
			return nil, err
		}
	}
	return &completionConfig, nil
}
